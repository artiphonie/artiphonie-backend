ARTIPHONIE - Serveur Web
================================================================

*Vous trouverez dans ce répertoire tout ce qui constitue notre serveur web, développé en Python.   
L'API, développé avec FastAPI, fournira des requêtes à la fois à l'interface web à destination des orthophonistes, et à l'application Godot, à destination des enfants.*